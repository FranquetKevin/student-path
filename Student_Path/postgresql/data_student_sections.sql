INSERT INTO student_sections (id_section, section_name) 
	VALUES ('EC-EI', 'Informatique de gestion');
INSERT INTO student_sections (id_section, section_name) 
	VALUES ('TE-BBSI', 'Sciences industrielles');
INSERT INTO student_sections (id_section, section_name) 
	VALUES ('PA-XB', 'Biologie médicale');
INSERT INTO student_sections (id_section, section_name) 
	VALUES ('EC-EM', 'Marketing');
INSERT INTO student_sections (id_section, section_name) 
	VALUES ('EC-ES', 'Assistant de direction');
INSERT INTO student_sections (id_section, section_name) 
	VALUES ('PA-CSI', 'Soins infirmiers');
INSERT INTO student_sections (id_section, section_name) 
	VALUES ('PA-CPSY', 'Psychomotricité');
INSERT INTO student_sections (id_section, section_name) 
	VALUES ('EC-ED', 'Droit');
INSERT INTO student_sections (id_section, section_name) 
	VALUES ('EC-ER', 'Commerce extérieur');
INSERT INTO student_sections (id_section, section_name) 
	VALUES ('EC-EC', 'Comptabilité');
INSERT INTO student_sections (id_section, section_name) 
	VALUES ('TE-MST', 'Textile');	
INSERT INTO student_sections (id_section, section_name) 
	VALUES ('PE-IANALL', 'Normale secondaire');	
INSERT INTO student_sections (id_section, section_name) 
	VALUES ('SO-AAS', 'Assistant(e) social(e)');
INSERT INTO student_sections (id_section, section_name) 
	VALUES ('SO-AIAS', 'Ingéniérie et action sociales');