INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (1, 11, 'Programmation de base', 1, 'EC-EI');
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (2, 5, 'Architecture Matérielle', 1, 'EC-EI');
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (3, 6,'Bases en gestion', 1, 'EC-EI');
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (4, 8,'Conception d''applications',1, 'EC-EI');
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (5, 5, 'Développement web', 2, 'EC-EI');
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (6, 9, 'E-business', 2, 'EC-EI');
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (7, 16, 'Programmation avancée', 2, 'EC-EI');
	
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (8, 6, 'Conception d''applications 2', 2, 'EC-EI');
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (9, 14, 'Programmation Orientée Objet', 1, 'EC-EI');
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (10, 5, 'Systèmes d''exploitation', 1, 'EC-EI');
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (11, 5, 'Gestion', 1, 'EC-EI');
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (12, 7, 'Méthodes statistiques', 2, 'EC-EI');
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (13, 7, 'Développement web et mobile', 2, 'EC-EI');
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (14, 6, 'Réseaux', 2, 'EC-EI');
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (15, 10, 'Labo pluridisciplinaire', 2, 'EC-EI');
	
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (16, 7, 'Admin Réseau et Système', 1, 'EC-EI');
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (17, 15, 'Architectures logicielles', 1, 'EC-EI');
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (18, 8, 'Savoir être, culture d''entreprise', 1, 'EC-EI');
INSERT INTO teaching_unites (id_teaching_unit, credits, teaching_unit_name, semester, fk_id_section) 
	VALUES (19, 30, 'Travail de fin d''études', 2, 'EC-EI');
	
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (1, 10, 0);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (2, 10, 0);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (3, 18, 0);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (4, 13, 0);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (5, 13, 0);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (7, 9, 0);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (7, 15, 0);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (8, 9, 1);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (8, 15, 1);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (9, 13, 1);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (9, 15, 1);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (10, 14, 1);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (10, 16, 0);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (11, 18, 0);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (13, 17, 0);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (15, 16, 0);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (15, 17, 0);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (17, 19, 1);
INSERT INTO dependencies (parent, child, dependencie_type) 
	VALUES (18, 19, 1);
