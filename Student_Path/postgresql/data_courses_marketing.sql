INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (20, 'C1-B1-Q1-UE1', 'Bases générales', 5, NULL, 1, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (21, 'C1-B1-Q1-UE2', 'Bases MKT', 5, NULL, 1, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (22, 'C1-B1-Q1-UE3', 'Organisation des entreprises et éléments de Management', 5, NULL, 1, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (23, 'C1-B1-Q1-UE4', 'SWOT', 7, NULL, 1, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (25, 'C1-B1-Q1-UE6', 'Langue choix', 8, NULL, 3, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (24, 'C1-B1-Q1-UE5', 'Langue obligatoire', 8, NULL, 3, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (26, 'C1-B1-Q2-UE7', 'Strat. Op''', 5, NULL, 2, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (27, 'C1-B1-Q2-UE8', 'Situation intégrative', 6, NULL, 2, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (28, 'C1-B1-Q2-UE9', 'Outils de gestion', 7, NULL, 2, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (29, 'C1-B1-Q2-UE10', 'Ostende/Düsseldorf', 4, NULL, 2, 'ec-m');
	
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (30, 'C1-B2-Q1-UE1', 'Simu 1', 9, NULL, 1, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (31, 'C1-B2-Q1-UE2', 'Traitement de données', 7, NULL, 1, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (32, 'C1-B2-Q1-UE3', 'Stratégie', 4, NULL, 1, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (33, 'C1-B2-Q1-UE4', 'Théorie du marketing', 4, NULL, 1, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (34, 'C1-B2-Q1-UE5', 'Langue obligatoire 2', 8, NULL, 3, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (35, 'C1-B2-Q1-UE6', 'Langue choix 2', 8, NULL, 3, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (36, 'C1-B2-Q2-UE7', 'Simu 2', 8, NULL, 2, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (37, 'C1-B2-Q2-UE8', 'Outils de gestion 2', 6, NULL, 2, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (38, 'C1-B2-Q2-UE9', 'Marketing opérationnel', 6, NULL, 2, 'ec-m');
	
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (39, 'C1-B3-Q1-UE1', 'Techniques de marketing', 7, NULL, 1, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (40, 'C1-B3-Q1-UE2', 'Négociation', 7, NULL, 1, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (41, 'C1-B3-Q1-UE3', 'Langues étrangères', 6, NULL, 1, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (42, 'C1-B3-Q1-UE4', 'Etudes de cas', 10, NULL, 1, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (43, 'C1-B3-Q1-UE5', 'TFE', 15, NULL, 2, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (44, 'C1-B3-Q1-UE6', 'Stage d''insertion professionnelle', 9, NULL, 2, 'ec-m');
INSERT INTO courses_packages (id_courses_package, matricule, package_name, credits, heures, quadrimestre, fk_id_section) 
	VALUES (45, 'C1-B3-Q1-UE7', 'Stage langue', 6, NULL, 2, 'ec-m');
	
