CREATE TABLE student_sections (
    id_section varchar(12) primary key,
    section_name varchar(100) not null
);

CREATE TABLE student_paths (
    id_student_path integer primary key
);

CREATE TABLE students (
    matricule varchar(7) primary key,
    starting_year integer not null,
    firstname varchar(25) not null,
    lastname varchar(50) not null,
    fk_id_student_path integer not null references student_paths (id_student_path),
    fk_id_section varchar(12) not null references student_sections (id_section)
);

CREATE TABLE courses_packages (
    id_courses_package integer primary key,
    matricule varchar(13) not null,
    package_name varchar(100) not null,
    credits integer not null,
    heures integer,
    quadrimestre integer not null default 1,
	status integer,
    fk_id_section varchar(12) not null references student_sections (id_section),
	unique(matricule, fk_id_section)
);

CREATE TABLE dependencies (
    parent integer references courses_packages(id_courses_package),
    child integer references courses_packages(id_courses_package),
    dependencie_type integer not null default 1,
    primary key(parent, child)
);

CREATE TABLE blocks (
    id_block integer primary key,
    number integer not null default 1,
    credits_total integer,
    credits_max integer not null default 60
);

CREATE TABLE courses_packages_in_blocks (
    fk_id_block integer not null references blocks(id_block),
    fk_id_courses_package integer not null references courses_packages(id_courses_package),
    primary key(fk_id_block, fk_id_courses_package)
);

CREATE TABLE blocks_in_student_paths (
    fk_id_student_path integer not null references student_paths(id_student_path),
    fk_id_block integer not null references blocks(id_block),
    primary key(fk_id_student_path, fk_id_block)
);