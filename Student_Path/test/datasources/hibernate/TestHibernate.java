/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datasources.hibernate;

import datasources.CoursesPackageManager;
import datasources.SectionManager;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.TeachingUnit;
import models.Section;
import models.Student;
import models.StudentPath;

/**
 *
 * @author Menini Thomas (d120041) <t.menini@student.helmo.be>
 */
public class TestHibernate implements AutoCloseable {

    HibernateConnectionManager hcm = new HibernateConnectionManager();
    CoursesPackageManager cpm = new HibernateCousesPackageManager(hcm);
    SectionManager sm = new HibernateSectionManager(hcm);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        TestHibernate hibernate = new TestHibernate();
        hibernate.testSaveAll();
        hibernate.testHashAndEqualsOfSectionShouldRespectProperty();
    }

    public HibernateConnectionManager getManager() {
        return hcm;
    }

    boolean testSaveAll() {
        boolean result = true;
        Student student;
        Section section;
        StudentPath path;

        TeachingUnit packages[] = {
            cpm.get(1),
            cpm.get(2),
            cpm.get(3),
            cpm.get(13)
        };
        section = sm.get("EC-EI");
        path = new StudentPath();
        path.createBlock(1, 60);
        path.addCoursesPackage(packages[0]);
        path.addCoursesPackage(packages[1]);
        path.addCoursesPackage(packages[2]);
        path.addCoursesPackage(packages[3]);
        student = new Student("D120042", section, "Thomas", "Menini", path);
        result = result && testSave(path);
        result = result && testSave(student);
        return result;
    }

    boolean testSave(Object object) {
        try {
            hcm.begin();
            hcm.save(object);
            hcm.commit();
            return true;
        } catch (Exception ex) {
            Logger.getLogger(TestHibernate.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    boolean testHashAndEqualsOfSectionShouldRespectProperty() {
        Section section1 = new Section("Informatique de gestion");
        Section section2 = new Section("Informatique de gestion");
        Section section3 = new Section("Management");
        System.out.println("Is " + section1.toString() + " equals to " + section2.toString() + " ?");
        if (section1.equals(section2)) {
            if (section1.hashCode() == section2.hashCode()) {
                System.out.println("Is " + section1.toString() + " equals to " + section3.toString() + " ?");
                if (section1.equals(section3)) {
                    if (section1.hashCode() == section3.hashCode()) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    @Override
    public void close() throws Exception {
        hcm.close();
    }
}
