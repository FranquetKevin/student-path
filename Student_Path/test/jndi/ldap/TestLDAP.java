/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jndi.ldap;

import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Thomas Menini
 */
public class TestLDAP {

    private static final String HOST = "ldap://ns-back.helmo.be:636";
    private final LDAP ldap;

    public TestLDAP() {
        ldap = new LDAP("dc=helmo,dc=be");
    }

    public boolean testConnect(String user, String password) {
        return ldap.connect(HOST, "uid=" + user + ",ou=Special Users,dc=helmo,dc=be", password);
    }

    public List<String[]> testSearchByID(String user, String password, String id) {
        if (testConnect(user, password)) {
            String[] attributes = {"entrydn", "givenname", "sn", "mail", "edupersonaffiliation"};
            ldap.clearFilter();
            ldap.addFilterAttribute("uid", id);
            List<String[]> data = ldap.search("ou=People", attributes);
            ldap.disconnect();
            return data;
        }
        return null;
    }

    public List<String[]> testSearchByName(String user, String password, String firstname, String lastname) {
        if (testConnect(user, password)) {
            String[] attributes = {"entrydn", "givenname", "sn", "mail", "edupersonaffiliation"};
            ldap.clearFilter();
            ldap.addFilterAttribute("givenname", firstname);
            ldap.addFilterAttribute("sn", lastname);
            List<String[]> data = ldap.search("ou=People", attributes);
            ldap.disconnect();
            return data;
        }
        return null;
    }

    public static void main(String[] args) {
        TestLDAP ldap = new TestLDAP();
        List<String[]> users;
        Scanner sc = new Scanner(System.in);
        System.out.print("Entrez un prénom et un NOM : ");
        String fullname = sc.nextLine();
        String[] names = fullname.split(" ", 2);
        users = ldap.testSearchByName("guest", "tho0wF6wLj", names[0], names[1]);
        for (String[] user : users) {
            System.out.printf("Entry : %s\n", user[0]);
            System.out.printf("Prenom : %s\n", user[1]);
            System.out.printf("Nom : %s\n", user[2]);
            System.out.printf("E-mail : %s\n", user[3]);
            System.out.printf("Type : %s\n", user[4]);
            System.out.println("---------------------");
        }
    }
}
