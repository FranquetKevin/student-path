package jndi.ldap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import javax.naming.ldap.LdapContext;

/**
 * Created by lsw on 21/03/14.
 */
public class LDAP {

    /**
     * The logger
     */
    public static final Logger logger = Logger.getLogger(LDAP.class.getName());
    
    private final String base;
    private final Hashtable<String, String> environment;
    private final Map<String,String> filterAttributes;
    private DirContext dirContext;
    private boolean connected = false;

    /**
     * Builder
     * @param _base the base path
     */
    public LDAP(String _base) {
        base = _base;
        environment = new Hashtable<>();
        filterAttributes = new HashMap<>();
    }

    /**
     * Connect a user to the sun database
     * @param _host the database
     * @param _user the user's data
     * @param _password the user's password
     * @return true if connected, false if error
     */
    public boolean connect(String _host, String _user, String _password) {
        try {
            environment.put(LdapContext.CONTROL_FACTORIES, "com.sun.jndi.ldap.ControlFactory");
            environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            environment.put(Context.PROVIDER_URL, _host);
            environment.put(Context.SECURITY_PROTOCOL, "ssl");
            environment.put(Context.SECURITY_AUTHENTICATION, "simple");
            environment.put(Context.SECURITY_PRINCIPAL, _user);
            environment.put(Context.SECURITY_CREDENTIALS, _password);
            environment.put(Context.STATE_FACTORIES, "PersonStateFactory");
            environment.put(Context.OBJECT_FACTORIES, "PersonObjectFactory");
            dirContext = new InitialDirContext(environment);
            connected = true;
            return true;
        } catch (NamingException ex) {
            logger.log(Level.WARNING, "Connexion Failed", ex);
            return false;
        }
    }

    /**
     * Is the database connected ?
     * @return true if yes
     */
    public boolean isConnected() {
        return connected;
    }

    /**
     * disconnect the database
     */
    public void disconnect() {
        try {
            if (isConnected()) {
                dirContext.close();
                connected = false;
            }
        } catch (NamingException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Add a filter on an attribute
     * @param attribute attribute
     * @param value value of the filter
     */
    public void addFilterAttribute(String attribute, String value) {
        filterAttributes.put(attribute, value);
    }
    
    /**
     * empty the filter
     */
    public void clearFilter() {
        filterAttributes.clear();
    }

    /**
     * Allows to search for an attribute
     * @param _ou 
     * @param _attributes
     * @return
     */
    public List<String[]>  search(String _ou, String[] _attributes) {
        try {
            String filter = buildFilter();
            SearchControls controls = new SearchControls();
            controls.setReturningAttributes(_attributes);
            controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            NamingEnumeration response = dirContext.search(_ou + "," + base, filter, controls);
            List<String[]> results = treatResponse(response, _attributes);
            if(results.isEmpty()) return null;
            return results;
        } catch (NamingException ex) {
            return null;
        }
    }
    
    private String buildFilter() {
        StringBuilder builder = new StringBuilder("(&(objectClass=organizationalPerson)");
        for (Map.Entry<String, String> filterAttribute : filterAttributes.entrySet()) {
            String attribute = filterAttribute.getKey();
            String value = filterAttribute.getValue();
            builder.append("(").append(attribute).append("=").append(value).append(")");
        }
        return builder.append(")").toString();
    }

    private List<String[]> treatResponse(NamingEnumeration response, String[] fields) throws NamingException {
        List<String[]> results = new ArrayList<>();
        while (response.hasMore()) {
            String[] result = new String[fields.length];
            SearchResult searchResult = (SearchResult) response.next();
            Attributes attributes = searchResult.getAttributes();
            int i = 0;
            for (String field : fields) {
                String key_value = attributes.get(field).toString();
                String key = field+": ";
                result[i++] = key_value.substring(key.length());
            }
            results.add(result);
        }
        System.out.println(results.toString());
        return results;
    }
}
