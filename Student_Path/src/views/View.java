/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import controllers.MasterController;
import controllers.ViewsController;
import java.awt.CardLayout;
import java.awt.Color;
import java.util.List;
import models.LevelAccess;
import models.TeachingUnit;
import views.Login;
import views.connecte.Connecte;
import views.connecte.PathViewer;

/**
 *
 * @author David
 */
public class View extends javax.swing.JFrame {

    private final MasterController master;
    private final CardLayout cardLayout;

    private final Login login;
    private final Connecte connected;
    private final ViewsController viewcontroller;
    /**
     * constructor of the class
     *
     * @param master the controller of the view
     * @param viewcontroller the controller of the view
     */
    public View(MasterController master, ViewsController viewcontroller) {
        this.master = master;
        this.viewcontroller= viewcontroller;
        initComponents();
        
        login = new Login(viewcontroller);
        connected = new Connecte(viewcontroller, master);
        cardLayout = new CardLayout();

        this.setLocationRelativeTo(getRootPane());
        initWindows();
        this.setVisible(true);

    }

    public void initWindows() {
        content.setLayout(cardLayout);
        content.add(login, "Login");
        content.add(getConnected(), "Connected");
        changeWindow("Login");

    }

    
    public void changeWindow(String window) {
        cardLayout.show(content, window);
    }

    public void changerCurrentConnectedWindow(String window) {
        getConnected().changeWindow(window);
    }

    public void changeConnectedWindow(String window) {
        getConnected().changeAccueil(window);
        changerCurrentConnectedWindow(window);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        content = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.GridBagLayout());

        content.setBackground(new java.awt.Color(255, 255, 255));
        content.setMinimumSize(new java.awt.Dimension(600, 600));
        content.setPreferredSize(new java.awt.Dimension(800, 600));
        content.setLayout(new java.awt.CardLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.2;
        getContentPane().add(content, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel content;
    // End of variables declaration//GEN-END:variables

    /**
     * @return the connected
     */
    public Connecte getConnected() {
        return connected;
    }

    
}
