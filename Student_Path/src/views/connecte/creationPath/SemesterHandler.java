/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.connecte.creationPath;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import models.TeachingUnit;

/**
 *
 * @author David
 */
public class SemesterHandler {

    private final DefaultListModel<String> coursModel;
    private final CreationPath creationPath;
    private final JLabel labelCredit;
    private final JList list;
    private int credits;
    private int indexSelected;

    /**
     * Builder
     * @param creationPath the object to create scholarship
     * @param liste a Jlist
     * @param labelCredit the labels
     */
    public SemesterHandler(CreationPath creationPath, JList liste, JLabel labelCredit) {
        this.list = liste;
        this.labelCredit = labelCredit;
        this.creationPath = creationPath;
        credits = 0;
        indexSelected = -1;
        changeCredits();

        coursModel = new DefaultListModel<>();
        liste.setModel(coursModel);

        setListener();

    }

    /**
     * add some courses to the semester
     * @param coursSelectionne
     */
    public void addCourses(TeachingUnit coursSelectionne) {
        coursModel.addElement(coursSelectionne.toString());

        credits += coursSelectionne.getCredits();
        changeCredits();
    }

    /**
     * remove a course
     * @param credits
     */
    public void removeSelectedCourse(int credits) {
        this.credits -= credits;
        coursModel.remove(indexSelected);

        changeCredits();
    }

    private void changeCredits() {
        labelCredit.setText("Crédits: " + credits);
    }

    private void removeCourseFromSemester() {
        if (indexSelected == -1) {
            return;
        }

        creationPath.removeCourse(coursModel.get(indexSelected), this);
    }


    private void listCoursMouseClicked(java.awt.event.MouseEvent evt) {
        if (indexSelected == list.getSelectedIndex()) {
            removeCourseFromSemester();
            indexSelected = -1;
        } else {
            indexSelected = list.getSelectedIndex();
        }
    }

    private void setListener() {
        list.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listCoursMouseClicked(evt);
            }
        });
    }

}
