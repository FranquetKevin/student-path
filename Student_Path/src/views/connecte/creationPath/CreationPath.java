/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views.connecte.creationPath;

import controllers.MasterController;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import models.TeachingUnit;
import models.Dependencie;

/**
 *
 * @author David
 */
public class CreationPath extends javax.swing.JPanel {

    private final MasterController master;
    private final DefaultListModel<String> coursModel;
    private final Map<Integer, SemesterHandler> quadrimestres;
    private List<TeachingUnit> cours;
    private int lastItemClicked;
    private int creditsTotal;

    /**
     * Builder
     * @param master mastercontroler of the view
     */
    public CreationPath(MasterController master) {
        this.master = master;
        initComponents();

        quadrimestres = new HashMap<>();
        initSemester();
        initParameters();

        coursModel = new DefaultListModel();
        listCours.setModel(coursModel);

    }

    private void initParameters() {
        lastItemClicked = -1;
        creditsTotal = 0;
        errorRequire.setVisible(true);
        errorTitle.setVisible(true);
    }

    private void initSemester() {
        quadrimestres.put(1, new SemesterHandler(this, listQuadri1, creditsLabel1));
        quadrimestres.put(2, new SemesterHandler(this, listQuadri2, creditsLabel2));
    }

    /**
     * set the availables courses
     * @param cours
     */
    public void setAvailableCourses(List<TeachingUnit> cours) {
        this.cours = cours;

        for (TeachingUnit uncours : cours) {
            coursModel.addElement(uncours.toString());
        }
    }
    
    /**
     * check if empty
     * @return
     */
    public boolean isAvailableCoursesEmpty() {
        return coursModel.isEmpty();
    }
    
    /**
     * clear the courses
     */
    public void clear() {
        coursModel.clear();
        quadrimestres.clear();
        initSemester();
        initParameters();
    }

    /**
     * remove a course from the semester
     * @param coursDeleted the course to deletion
     */
    public void removeCourseFromSemester(TeachingUnit coursDeleted) {
        cours.add(coursDeleted);
        coursModel.addElement(coursDeleted.toString());
        creditsTotal -= coursDeleted.getCredits();
        actualiseYearCredits();
    }

    /**
     * remove a course using his name
     * @param nomCours the name of the course to remove
     * @param quadrimestre the semester of the course to delete
     */
    public void removeCourse(String nomCours, SemesterHandler quadrimestre) {
        TeachingUnit unCours = findCours(nomCours);
        if (unCours == null) {
            showError("Cours introuvable", "Introuvable");
            return;
        }

        if (master.removeCoursePath(unCours)) {
            creditsTotal -= unCours.getCredits();
            coursModel.addElement(unCours.toString());
            quadrimestre.removeSelectedCourse(unCours.getCredits());
            actualiseYearCredits();

        } else {
            showError("Vous ne pouvez pas supprimer ce cours", "Impossible");
        }
    }

    private TeachingUnit findCours(String nomCours) {
        for (TeachingUnit unCours : cours) {
            if (nomCours.equals(unCours.toString())) {
                return unCours;
            }
        }
        return null;
    }

    private void addCourseToSemesterFromList() {
        TeachingUnit coursSelectionne = findCours(coursModel.get(lastItemClicked));

        addCourseToSemester(coursSelectionne);

        actualiseYearCredits();
    }
    
    public void setCourseToSemester(List<TeachingUnit> _cours){
        for (TeachingUnit teachingUnit : _cours){
            cours.add(teachingUnit);
            addTeachingUnitToListSemester(teachingUnit);
            actualiseYearCredits();
        }
    }
    
    private void addCourseToSemester(TeachingUnit coursSelected){
       
        if(creditsTotal + coursSelected.getCredits() > 60){
           String impossible = "Les critères ne sont pas remplis";
            showError("Vous ne pouvez pas avoir plus de 60 crédits", impossible );
            return;
       }
        if (!master.addCoursePath(coursSelected)) {

            return;
        }
       
        else{
            coursModel.removeElementAt(lastItemClicked);
            addTeachingUnitToListSemester(coursSelected);
        }
        
    }
    
    private void addTeachingUnitToListSemester(TeachingUnit cours){
        creditsTotal += cours.getCredits();
        SemesterHandler qh = quadrimestres.get(cours.getSemester());
        qh.addCourses(cours);
    }

    private String getDependancesParent(TeachingUnit coursSelectionne) {
        StringBuilder builder = new StringBuilder();

        //builder.append("\nDépendances:");
        for (Dependencie dependance : coursSelectionne.getDependencies()) {  
            if(builder.length() == 0){
                builder.append(" ").append(dependance.getParent().getName());
            }else{
                builder.append(" + ").append(dependance.getParent().getName());
            }            
        }
        return builder.toString();

    }


    private void actualiseYearCredits() {
        creditsYear.setText(String.valueOf(creditsTotal));
        barCreditsYear.setValue(creditsTotal);
    }

    private void showError(String msg, String title) {
        JOptionPane.showMessageDialog(this, msg, title, JOptionPane.ERROR_MESSAGE);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listCours = new javax.swing.JList();
        errorTitle = new javax.swing.JLabel();
        errorRequire = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        listQuadri1 = new javax.swing.JList();
        creditsLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        listQuadri2 = new javax.swing.JList();
        creditsLabel2 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        barCreditsYear = new javax.swing.JProgressBar();
        jPanel8 = new javax.swing.JPanel();
        creditsYear = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        validate = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(new java.awt.GridBagLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(153, 153, 153));
        jLabel1.setText("Cours");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(4, 9, 5, 0);
        jPanel1.add(jLabel1, gridBagConstraints);

        listCours.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listCoursMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(listCours);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.2;
        gridBagConstraints.insets = new java.awt.Insets(0, 9, 4, 9);
        jPanel1.add(jScrollPane1, gridBagConstraints);

        errorTitle.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        errorTitle.setForeground(new java.awt.Color(255, 0, 0));
        errorTitle.setText("Requiert: ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(4, 9, 4, 0);
        jPanel1.add(errorTitle, gridBagConstraints);

        errorRequire.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        errorRequire.setForeground(new java.awt.Color(255, 0, 0));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 4, 0);
        jPanel1.add(errorRequire, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.weightx = 0.2;
        gridBagConstraints.weighty = 0.6;
        gridBagConstraints.insets = new java.awt.Insets(14, 16, 0, 16);
        add(jPanel1, gridBagConstraints);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new java.awt.GridBagLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(153, 153, 153));
        jLabel4.setText("Quadrimestre 1");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 6, 0, 6);
        jPanel3.add(jLabel4, gridBagConstraints);

        jScrollPane2.setViewportView(listQuadri1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 9, 0, 9);
        jPanel3.add(jScrollPane2, gridBagConstraints);

        creditsLabel1.setText("label");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel3.add(creditsLabel1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 26, 26);
        jPanel2.add(jPanel3, gridBagConstraints);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel4.setLayout(new java.awt.GridBagLayout());

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(153, 153, 153));
        jLabel6.setText("Quadrimestre 2");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        jPanel4.add(jLabel6, gridBagConstraints);

        jScrollPane3.setViewportView(listQuadri2);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 9, 0, 9);
        jPanel4.add(jScrollPane3, gridBagConstraints);

        creditsLabel2.setText("jLabel5");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel4.add(creditsLabel2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 26, 0);
        jPanel2.add(jPanel4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.weighty = 0.6;
        gridBagConstraints.insets = new java.awt.Insets(30, 16, 18, 16);
        add(jPanel2, gridBagConstraints);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setLayout(new java.awt.GridBagLayout());

        barCreditsYear.setMaximum(75);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        jPanel7.add(barCreditsYear, gridBagConstraints);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setLayout(new java.awt.GridBagLayout());

        creditsYear.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        creditsYear.setForeground(new java.awt.Color(153, 153, 153));
        creditsYear.setText("0");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel8.add(creditsYear, gridBagConstraints);

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(153, 153, 153));
        jLabel13.setText("Crédits:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel8.add(jLabel13, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 5, 0);
        jPanel7.add(jPanel8, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(30, 16, 0, 16);
        add(jPanel7, gridBagConstraints);

        validate.setBackground(new java.awt.Color(51, 255, 51));
        validate.setText("Sauvegarder");
        validate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validateActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 6, 16);
        add(validate, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void listCoursMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listCoursMouseClicked
        if (lastItemClicked == listCours.getSelectedIndex() && lastItemClicked != -1) {
            addCourseToSemesterFromList();
            lastItemClicked = -1;
            
        } else {
            lastItemClicked = listCours.getSelectedIndex();
            TeachingUnit coursSelected = findCours(coursModel.get(lastItemClicked));
            errorRequire.setText(getDependancesParent(coursSelected));
        }
    }//GEN-LAST:event_listCoursMouseClicked

    private void validateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_validateActionPerformed
       master.validatePath();
    }//GEN-LAST:event_validateActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barCreditsYear;
    private javax.swing.JLabel creditsLabel1;
    private javax.swing.JLabel creditsLabel2;
    private javax.swing.JLabel creditsYear;
    private javax.swing.JLabel errorRequire;
    private javax.swing.JLabel errorTitle;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JList listCours;
    private javax.swing.JList listQuadri1;
    private javax.swing.JList listQuadri2;
    private javax.swing.JButton validate;
    // End of variables declaration//GEN-END:variables
}
