package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Thomas
 */
@Entity
@Table(name = "teaching_unites")
public class TeachingUnit implements Serializable{

    @Id @Column(name="id_teaching_unit")
    @org.hibernate.annotations.GenericGenerator(name = "hilo-strategy", strategy = "hilo")
    @GeneratedValue(generator = "hilo-strategy")
    private Integer id;
    @Column(name="teaching_unit_name")
    private final String name;
    private final int credits;
    private final int semester;
    private final int block;
    @ManyToOne(optional = false)
    @JoinColumn(name = "fk_id_section")
    private final Section section;
    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "dependencie.child")
    private final List<Dependencie> dependencies;

    /**
     * Default builder
     */
    protected TeachingUnit() {
        name = "";
        credits = 0;
        semester = 0;
        block= 0;
        section = new Section();
        dependencies = new ArrayList<>();
    }
    
    /**
     * Dedicated builder
     * @param name the name of the learning unit
     * @param credits the nulber of credits of the LU
     * @param semester the semester where the LU is active
     * @param block the block number of the learning unit
     * @param section the section of the learning unit
     */
    public TeachingUnit(String name, int credits, int semester, int block, Section section) {
        this.name = name;
        this.credits = credits;
        this.semester = semester;
        this.block = block;
        this.section = section;
        dependencies = new ArrayList<>();
    }

    /**
     * Get the name of the package
     * @return the name of the package
     */
    public String getName() {
        return name;
    }

    /**
     * get the number of credits of the coursespackage
     * @return the credits
     */
    public int getCredits() {
        return credits;
    }
    
    /**
     * get the section of the teaching unit
     * @return the section
     */
    
    public Section getSection(){
        return section;
    }
 /**
     * get the block number of the teaching unit
     * @return the section
     */
    
    public int getBlock(){
        return block;
    }
    /**
     * get a list of all the dependencies of the coursespackage
     * @return the dependencies
     */
    public List<Dependencie> getDependencies() {
        return dependencies;
    }

    /**
     * add a new dependencie to a coursesPackage
     * @param coursesPackage the other coursespackage
     * @param type the type of the dependencie
     */
    public void addDependencie(TeachingUnit coursesPackage, TypeDepencie type) {
        dependencies.add(new Dependencie(coursesPackage, this, type));
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TeachingUnit)) {
            return false;
        }
        TeachingUnit cp = (TeachingUnit) obj;
        return (id.equals(cp.id)) && (credits == cp.credits);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.id);
        hash = 17 * hash + Objects.hashCode(this.name);
        hash = 17 * hash + this.credits;
        return hash;
    }

    @Override
    public String toString() {
        return "[ Q" + semester + "]   "+  name + " [" + credits + " crédits] [Bloc :"+block +"]";
    }

    /**
     * @return the semester
     */
    public int getSemester() {
        return semester;
    }
}
