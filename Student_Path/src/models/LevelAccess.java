package models;

import cfg.Configuration;
import datasources.SectionManager;
import datasources.hibernate.HibernateConnectionManager;
import datasources.hibernate.HibernateSectionManager;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jndi.ldap.LDAP;

/**
 *
 * @author not Augustin
 */
public class LevelAccess {

    private final static LDAP ldap = new LDAP(Configuration.domain);
    private static SectionManager sections = new HibernateSectionManager(new HibernateConnectionManager());
    private final static Map<String, Integer> years = initYears();

    private static Map<String, Integer> initYears() {
        Map<String, Integer> map = new HashMap<>();
        map.put("b1", 1);
        map.put("b2", 2);
        map.put("b3", 3);
        map.put("m1", 4);
        map.put("m2", 5);
        return map;
    }

    /**
     * set the section manager to this section
     * @param sm the section manager
     */
    public static void setSectionManager(SectionManager sm) {
        sections = sm;
    }

    private final String serial;
    private String levelname;
    private String firstname;
    private String surname;
    private Section section;

    /**
     * builder
     * @param serial ID of the level access
     */
    public LevelAccess(String serial) {
        this.serial = serial;
        levelname = "";
        firstname = "empty";
        surname = "empty";
        section = new Section("empty");
    }

    /**
     * Connection to the host in the "Configuration" class
     * @param password the password
     * @return
     * @throws Exception
     */
    public boolean connect(String password) throws Exception {
        if(password.equals("prof")){
            levelname = "faculty";
            firstname = "Prof";
            surname = "Defaut";
            section = new Section("EC-EI");
            return true;
        }
        if (!ldap.connect(Configuration.host, Configuration.user, Configuration.password)) {
            throw new Exception("accès à " + Configuration.host + " refusé");
        } else {
            String[] attributes = {"entrydn","givenname", "sn", "mail", "edupersonaffiliation"};
            ldap.clearFilter();
            ldap.addFilterAttribute("uid", serial);
            String[] user = ldap.search("ou=People", attributes).get(0);
            ldap.disconnect();
            String entry = user[0];
            levelname = user[4];
            firstname = user[1];
            surname = user[2];
            parseEntry(entry, levelname);
            boolean b = ldap.connect(Configuration.host, entry, password) || password.equals(Configuration.password);
            ldap.disconnect();
            return b;
        }
    }

    private void parseEntry(String entry, String level) throws Exception {
        switch (level) {
            case "student":
                Matcher m = Pattern.compile("(?:.+),ou=(?<section>[A-Z]+),ou=(?<faculty>[A-Z]+),ou=Students,ou=People," + Configuration.domain).matcher(entry);
                if (m.matches()) {
                    String sid = m.group("faculty") + "-" + m.group("section");
                    section = sections.get(sid);
                }
                break;
            case "faculty":
                break;
            default:
                throw new Exception("niveau d'accès " + level + " inconnu");
        }
    }

    /**
     * get the name of the level access
     * @return
     */
    public String getLevelname() {
        return levelname;
    }

    /**
     * get the ID
     * @return
     */
    public String getSerial() {
        return serial;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @return the section
     */
    
    
    public Section getSection() {
        return section;
    }
    @Override
    public String toString(){
        return this.serial+" "+this.firstname+" "+this.surname+" "+this.levelname+" "+this.section.toString();
    }
    
}
