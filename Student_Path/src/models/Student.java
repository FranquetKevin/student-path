package models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author not Augustin
 */
@Entity
@Table(name = "students")
public class Student implements Serializable {

    @Id
    private final String serial;
    @ManyToOne(optional = false)
    @JoinColumn(name = "fk_id_section")
    private final Section section;
    private final String firstname;
    private final String lastname;
    // private final boolean validated;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_id_student_path")
    private StudentPath path;

    /**
     * builder
     */
    protected Student() {
        serial = "";
        section = new Section();
        firstname = "";
        lastname = "";
        //validated = false;
        path = new StudentPath();
    }

    /**
     * other builder
     * @param serial the ID of the student
     * @param section the section of the student
     * @param firstname the firstname of the student
     * @param lastname the lastname of the student
     * @param path the scholarship of the student
     */
    public Student(String serial, Section section, String firstname, String lastname, StudentPath path) {
        this.serial = serial;
        this.section = section;
        this.firstname = firstname;
        this.lastname = lastname;
        //this.validated = validated;
        this.path = path;
    }

    /**
     * get student ID
     * @return
     */
    public String getSerial() {
        return serial;
    }

    /**
     * get the name of the student's section
     * @return
     */
    public String getSectionName() {
        return section.getSectionName();
    }

    /**
     * get the section
     * @return
     */
    public Section getSection() {
        return section;
    }
    
    /**
     * get the full name
     * @return lastname+firstname
     */
    public String getFullName() {
        return new StringBuilder(lastname).append(" ").append(firstname).toString();
    }

    /*
     public boolean isValidated() {
     return validated;
     }
     */

    /**
     * get scholorship
     * @return
     */
    
    public StudentPath getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(StudentPath path) {
        this.path = path;
    }
    
    @Override
    public String toString(){
        return this.firstname + " " + this.lastname;
    }
    
    @Override
    public int hashCode(){
        int hashcode = this.firstname.hashCode();
        hashcode += this.lastname.hashCode();
        return hashcode;
    }
    
    @Override
    public boolean equals(Object object){
        if(object instanceof Student){
            Student student = (Student)object;
            if(student.serial.equals(this.serial) && student.firstname.equals(this.firstname) && student.lastname.equals(this.lastname))
                return true;
        }
        return false;
    }

    
}
