package models;

import java.io.Serializable;
import java.util.*;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author not Augustin
 */
@Entity
@Table(name = "student_paths")
public class StudentPath implements Serializable {

    @Id
    @Column(name = "id_student_path")
    @org.hibernate.annotations.GenericGenerator(name = "hilo-strategy", strategy = "hilo")
    @GeneratedValue(generator = "hilo-strategy")
    private Integer id;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "year_planning_in_student_paths",
            joinColumns = {@JoinColumn(name = "fk_id_student_path")},
            inverseJoinColumns = {@JoinColumn(name = "fk_id_year_planning")}
    )
    @OrderBy("year")
    private List<YearPlanning> path;
    
    @Transient
    private YearPlanning currentYear;

    /**
     * builder
     */
    public StudentPath() {
        path = new ArrayList<YearPlanning>();
    }
    
    /**
     * get the next year planning
     * @return 
     */
    public YearPlanning getCurrentYearPlanning() {
        if(currentYear == null){
            if(!path.isEmpty()){
                currentYear = path.get(path.size()-1);
            }
            else{
                currentYear = new YearPlanning();
                path.add(currentYear);
            }
        }
        return this.currentYear;
    }
    
    public List<YearPlanning> getPath(){
        return this.path;
    }

    /**
     * add a learning unit to a year
     * @param blockNumber the year concerned
     * @param coursesPackage the learning unit concerned
     * @return
     */
    public boolean addCoursesPackage(TeachingUnit coursesPackage) {
        if (!checkDependencies(coursesPackage)) {
            return false;
        }
        
        return currentYear.addPackage(coursesPackage);
    
    }
    
    /**
     * remove a learning unit of a year
     * @param blockNumber
     * @param coursesPackage
     * @return
     */
    public boolean removeCoursesPackage(TeachingUnit coursesPackage){
        if(!checkDependicesForDelete(coursesPackage)){
            return false;
        }
        
        return currentYear.removePackage(coursesPackage);
    }

    /**
     * Create a new year with a maximum credit limit
     * @param creditsMax the maximuum of the credit
     * @return
     */
    public YearPlanning createBlock(int year, int creditsMax) {
        currentYear = new YearPlanning(year, creditsMax);
        path.add(currentYear);
        return this.currentYear;
    }
    
    private boolean checkDependencies(TeachingUnit coursesPackage) {
        List<Dependencie> dependencies = coursesPackage.getDependencies();
        if (dependencies.isEmpty()) {
            return true;
        }
        for (Dependencie dependencie : dependencies) {
            boolean found = false;
            switch (dependencie.getType()) {
                case PRE_REQUISITES:
                    found = checkDependencieForSuccessfulTeachingUnits(dependencie);
                    break;
                case CO_REQUISITES:
                    found = checkCoursesPackageForYearPlanning(dependencie.getParent());
                    break;
            }
            if (!found) {
                return false;
            }
        }
        return true;
    }
    
    private boolean checkDependicesForDelete(TeachingUnit unCours){      
        if(currentYear == null){
            return false;
        }
        
        for(TeachingUnitState cours: currentYear){
            for (Dependencie dependDe : cours.getUnit().getDependencies()) {
                    if (unCours.getName().equals(dependDe.getParent().getName())) {
                        return false;
                    }
            }
        }        
        
        return true;        
    }

    private boolean checkDependencieForSuccessfulTeachingUnits(Dependencie dependencie) {
        for(YearPlanning aYear : this.path){
            for(TeachingUnitState aUnit : aYear){
                if (aUnit.getSuccessful() == true && aUnit.getUnit().getName().equals(dependencie.getParent().getName())) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean checkCoursesPackageForYearPlanning(TeachingUnit coursesPackage) {
        return currentYear.containsPackage(coursesPackage);
    }
    
    @Override
    public String toString(){
        return this.id+" "+this.currentYear.toString();
    }
    
    
}
