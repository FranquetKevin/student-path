/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.List;

/**
 *
 * @author David
 */
public class StudentsList extends java.util.Observable {

    private List<Student> students;

    /**
     * set the list
     * @param students
     */
    public void setStudents(List<Student> students) {
        this.students = students;
        setChanged();
        notifyObservers();
    }
    
    /**
     * get the list
     * @return
     */
    public List<Student> getStudents(){
        return students;
    }
    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        for(Student student : students){
            builder.append(student.toString()+"\n");
        }
        return builder.toString();
    }
}
