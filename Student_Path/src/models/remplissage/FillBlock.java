/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.remplissage;

import java.awt.Color;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map;
import java.util.Observer;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import models.YearPlanning;
import models.TeachingUnitState;
import views.connecte.Course;

/**
 *
 * @author David
 */
public class FillBlock {

    private Map<Integer, JPanel> listPanels;
    private Integer[] credits;
    private JLabel[] creditsTot;
    private final Dimension dimensionMax;
    private boolean isStudent;
    /**
     * builder
     * @param isStudent true if he user is a student
     */
    public FillBlock(boolean isStudent) {
        dimensionMax = new Dimension(5, 5);
        this.isStudent = isStudent;
    }

    /**
     * Fill the panels with the informations
     * @param block the year with the informations
     * @param quadrimestres the panels to fill
     * @param creditsTot the labels to write into
     */
    public void fillCourses(Observer observer,YearPlanning block, JPanel[] quadrimestres, JLabel[] creditsTot) {
        this.creditsTot = creditsTot;
        listPanels = new HashMap<>();
        int taille = quadrimestres.length;
        credits = new Integer[taille];

        for (int i = 1; i < taille; i++) {
            credits[i] = 0;
            JPanel panel = new JPanel();
            panel.setBackground(Color.WHITE);
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
            listPanels.put((Integer)i,panel);
        }

        for (TeachingUnitState unCours : block) {
            addCourses(observer,unCours);
            
        }

        for (int i = 1; i < taille; i++) {
            JScrollPane scroll = new JScrollPane(listPanels.get(i));
            scroll.setBorder(null);
            quadrimestres[i].removeAll();
            quadrimestres[i].add(scroll);
            creditsTot[i].setText("Crédits: " + credits[i]);
        }
    }

    private void addCourses(Observer observer,TeachingUnitState unCours) {
        int quadrimestre = unCours.getUnit().getSemester();
        JPanel espacement = new JPanel();
        espacement.setMaximumSize(dimensionMax);
        JPanel panel = listPanels.get(quadrimestre);
        panel.add(espacement);
        
        //cette partie sert au radiobutton, afin qu'il soit dans 
        //un état correct à l'arrivée, et non modifiable par l'étudiant
        Course course = new Course(unCours.toString());
        
        course.setRadioButtonsEnabled(!this.isStudent);
        course.setIsSuccessfull(unCours.getSuccessful());
        
        course.addOberver(observer);
        course.setTeachingUnitToEvent(unCours);
        
        panel.add(course);
        int credit = credits[quadrimestre] + unCours.getUnit().getCredits();
        credits[quadrimestre] = credit;
    }

}
