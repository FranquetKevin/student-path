/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Observable;

/**
 *
 * @author Augustin
 */
public class YearSuccessChangedEvent extends Observable{
    private boolean YearIsSuccess;
    private TeachingUnitState unit;
    public void setSuccess(boolean success){
        if(this.YearIsSuccess != success){
            this.YearIsSuccess = success;
            this.setChanged();
        }
    }
    public boolean isYearSuccess(){
        return this.YearIsSuccess;
    }
    
    public void setUnit(TeachingUnitState unit){
        this.unit = unit;
    }
    public TeachingUnitState getUnit(){
        return this.unit;
    }
}
