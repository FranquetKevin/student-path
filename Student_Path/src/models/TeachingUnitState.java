package models;
import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "teaching_unites_state")
public class TeachingUnitState implements Serializable{

    @Id @Column(name = "id_teaching_unites_state")
    @org.hibernate.annotations.GenericGenerator(name = "hilo-strategy", strategy = "hilo")
    @GeneratedValue(generator = "hilo-strategy")
    private int id;
    
    @ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.REMOVE} )
    private final YearPlanning planning;
    
    @ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.REMOVE} )
    private final TeachingUnit unit;
    
    @Column(name="successful")
    private boolean successful;
    
    public TeachingUnitState(){
        unit = new TeachingUnit();
        planning = new YearPlanning();
        successful = false;
    }
    
    public TeachingUnitState(TeachingUnit unit, YearPlanning planning){
        this.unit = unit;
        this.planning = planning;
        successful = false;
    }   
    
    public void isSuccessful(boolean success){
        successful = success;
    }
    
    public int getId(){
        return id;
    }
    
    public TeachingUnit getUnit(){
        return unit;
    }
    
    public boolean getSuccessful(){
        return successful;
    }
    
    @Override
    public String toString(){
        return unit.toString();
    }
}
