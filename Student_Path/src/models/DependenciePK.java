/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package models;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 *
 * @author Menini Thomas (d120041) <t.menini@student.helmo.be>
 */
@Embeddable
public class DependenciePK implements Serializable {
    @OneToOne
    @JoinColumn(name = "parent")
    private TeachingUnit parent;
    @OneToOne
    @JoinColumn(name = "child")
    private TeachingUnit child;

    /**
     * builder
     */
    public DependenciePK() {}

    /**
     * other builder
     * @param parent the parent in the dependencie
     * @param child the child in the dependencie
     */
    public DependenciePK(TeachingUnit parent, TeachingUnit child) {
        this.parent = parent;
        this.child = child;
    }

    /**
     * return the parent
     * @return
     */
    public TeachingUnit getParent() {
        return parent;
    }

    /**
     * return the child
     * @return
     */
    public TeachingUnit getChild() {
        return child;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof DependenciePK)  {
            DependenciePK pk = (DependenciePK) obj;
            return (parent.equals(pk.parent)) 
                    && (child.equals(pk.child));
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = parent.hashCode();
        hash = hash + child.hashCode();
        return hash + super.hashCode();
    }
    @Override
    public String toString(){
        return this.parent.toString()+" parent of "+this.child.toString();
    }
    
}
