/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 *
 * @author Menini Thomas (d120041) <t.menini@student.helmo.be>
 */
@Entity
@Table(name = "dependencies")
@AssociationOverrides({
    @AssociationOverride(name = "dependencie.parent", joinColumns = @JoinColumn(name = "parent")),
    @AssociationOverride(name = "dependencie.child", joinColumns = @JoinColumn(name = "child"))
})
public class Dependencie implements Serializable {

    @Id
    private DependenciePK dependencie;
    @Column(name = "dependencie_type")
    private TypeDepencie type;

    /**
     * builder
     */
    protected Dependencie() {
        dependencie = new DependenciePK();
        type = type.PRE_REQUISITES;
    }

    /**
     * other builder
     * @param parent the coursesPackage of origins
     * @param child the coursespackage of child in the relation
     * @param type the type of relation
     */
    public Dependencie(TeachingUnit parent, TeachingUnit child, TypeDepencie type) {
        dependencie = new DependenciePK(parent, child);
        this.type = type;
    }

    /**
     * get the parents of the dependencie
     * @return
     */
    public TeachingUnit getParent() {
        return dependencie.getParent();
    }

    /**
     * get the child of the dependencie
     * @return
     */
    public TeachingUnit getChild() {
        return dependencie.getChild();
    }

    /**
     * the type of the dependencie
     * @return
     */
    public TypeDepencie getType() {
        return type;
    }
    
    @Override
    public String toString(){
        String answer;
        answer = this.dependencie.toString()+" "+this.type.toString();
        return answer;
    }
}
