package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Note from augustin : I think that block means year, but I am unsure.
 * @author Thomas
 */
@Entity
@Table(name = "year_planning")
public class YearPlanning implements Serializable, Iterable<TeachingUnitState>{

    @Id @Column(name = "id_year_planning")
    @org.hibernate.annotations.GenericGenerator(name = "hilo-strategy", strategy = "hilo")
    @GeneratedValue(generator = "hilo-strategy")
    private Integer id;
    @Column(name = "year_of_planning")
    private final int year;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "join_year_planning_teaching_unit",
            joinColumns = {@JoinColumn(name = "fk_id_year_planning")},
            inverseJoinColumns = {@JoinColumn(name = "fk_id_teaching_unites_state")}
    )
    private final List<TeachingUnitState> packages;
    @Column(name="credits_total")
    private int creditsTotal;
    @Column(name="credits_max")
    private final int creditsMax;
    private TypeYearPlanning status;

    /**
     * builder
     */
    protected YearPlanning() {
        id = new Integer(0);
        year = 1;
        packages = new ArrayList<>();
        creditsTotal = 0;
        creditsMax = 60;
        status = TypeYearPlanning.IN_PROGRESS;
    }

    /**
     * Builder
     * @param year the year of the planning
     * @param creditsMax the credits max for a block
     */
    public YearPlanning(int year, int creditsMax) {
        this.year = year;
        packages = new ArrayList<>();
        creditsTotal = 0;
        this.creditsMax = creditsMax;
        status = TypeYearPlanning.IN_PROGRESS;
    }

    /**
     * get the number of the block
     * @return an int
     */
    public int getNumber() {
        return year;
    }

    /**
     * get the total of credits for the block
     * @return
     */
    public int getCreditsTotal() {
        return creditsTotal;
    }

    /**
     * return the maximal number of credits for the block
     * @return
     */
    public int getCreditsMax() {
        return creditsMax;
    }
    
    public List<TeachingUnitState> getPackages(){
        return packages;
    }
    
    public TypeYearPlanning getStatus(){
        return status;
    }
    
    public void setStatus(TypeYearPlanning status){
        this.status = status;
    }

    /**
     * add a course package to the block
     * @param coursesPackage the courses to add
     * @return true if OK
     */
    public boolean addPackage(TeachingUnit coursesPackage) {
        if (creditsTotal + coursesPackage.getCredits() > creditsMax) {
            return false;
        }
        this.packages.add(new TeachingUnitState(coursesPackage, this));
        creditsTotal += coursesPackage.getCredits();
        return true;
    }

    /**
     * remove the coursepackege from the block
     * @param coursesPackage
     * @return
     */
    public boolean removePackage(TeachingUnit coursesPackage) {
        if (creditsTotal - coursesPackage.getCredits() < 0) {
            return false;
        }
        
        for(TeachingUnitState unit:packages){
            if(unit.getUnit().equals(coursesPackage)){
                packages.remove(unit);
            }
        }
        creditsTotal -= coursesPackage.getCredits();
        return true;
    }

    /**
     * check if the block contains the specified coursespackage
     * @param coursesPackage the coursespackage to check
     * @return true if block already contains a coursespackage similar
     */
    public boolean containsPackage(TeachingUnit coursesPackage) {
        for (TeachingUnitState pack : packages) {
            if (pack.getUnit().equals(coursesPackage)) {
                return true;
            }
        }
        return false;
    }
    
    public void removeAllPackage(){
        this.packages.clear();
        creditsTotal = 0;
    }

    @Override
    public Iterator<TeachingUnitState> iterator() {
        return packages.listIterator();
    }
    
    @Override
    public String toString(){
        return this.id+" "+this.creditsTotal+"/"+this.creditsMax+" "+this.year;
    }
}
