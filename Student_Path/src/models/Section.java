package models;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author not Augustin
 */
@Entity
@Table(name = "student_sections")
public class Section implements Serializable {

    @Id @Column(name = "id_section")
    @org.hibernate.annotations.GenericGenerator(name = "hilo-strategy", strategy = "hilo")
    @GeneratedValue(generator = "hilo-strategy")
    private String id;
    @Column(name = "section_name")
    private final String sectionname;

    /**
     * builder
     */
    protected Section() {
        this("noSectionName","noId");
    }
    
    /**
     * other builder
     * @param sectionname the name of the section
     */
    public Section(String sectionname) {
        this(sectionname,"noId");
    }

    public Section(String sectionname_,String id_){
        
        if(sectionname_ != null && !sectionname_.isEmpty()){
            this.sectionname = sectionname_;
        }
        else{
            this.sectionname ="noSectionName";
        }
        if(id_ != null && !id_.isEmpty()){
            this.id = id_;
        }
        else{
            this.id = "noId";
        }
    }
    
    /**
     * get the id of the section
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * get the name of the section
     * @return
     */
    public String getSectionName() {
        return sectionname;
    }
    
    @Override
    public String toString(){
        return this.sectionname;
    }
    
    @Override
    public int hashCode(){
        if(id==null && sectionname!=null){
            return sectionname.hashCode();
        }
        if(sectionname==null && id != null){
            return id.hashCode();
        }
        if(id==null && sectionname==null){
            return this.toString().hashCode();
        }
        return this.id.hashCode() + this.sectionname.hashCode();
    }
    
    @Override
    public boolean equals(Object object){
        if(object instanceof Section){
            Section section = (Section)object;
            if(this.id != null && section.id != null){
                if(section.id.equals(this.id) && section.sectionname.equals(this.sectionname))
                return true;
            }
            else if(this.id == null && section.id == null){
                if(section.sectionname.equals(this.sectionname))
                return true;
            }
            
        }
        return false;
    }
}
