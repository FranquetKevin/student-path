/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cfg;

/**
 *
 * @author Thomas Menini
 */
public class Configuration {

    /**
     * the host of the datas about the students and the teachers
     */
    public static final String host = "ldap://ns-back.helmo.be:636";

    /**
     * the domain where to find the datas
     */
    public static final String domain = "dc=helmo,dc=be";

    /**
     * the informations about a user
     */
    public static final String user = "uid=guest,ou=Special Users,"+domain;

    /**
     * the password of the user
     */
    public static final String password = "tho0wF6wLj";

    /**
     *
     */
    public static final int credits_max_block = 60;
}
