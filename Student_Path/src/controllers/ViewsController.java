/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.awt.CardLayout;
import java.awt.Color;
import java.util.List;
import models.LevelAccess;
import models.TeachingUnit;
import views.Login;
import views.View;
import views.connecte.Connecte;
import views.connecte.PathViewer;
import models.business.*;

/**
 *
 * @author David
 */
public class ViewsController  {

    private final MasterController master;
    private final Login login;
    private final View view;
    /**
     * constructor of the class
     *
     * @param master the controller of the view
     */
    public ViewsController(MasterController master) {
        this.master = master;
//      initComponents();
        view = new View(master, this);
        login = new Login(this);
        
        

    }


    /**
     * This method allows to perform an action
     *
     * @param action the name of the action
     */
    public void action(String action) {
        switch (action) {
            case "CreationPath":
                if ( view.getConnected().getCreationPath().isAvailableCoursesEmpty()) {
                     view.getConnected().getCreationPath().setAvailableCourses(master.checkAvailableCourse(false));
                     view.getConnected().getCreationPath().setCourseToSemester(master.checkAvailableCourse(true));
                }
                break;
            case "Valider":
                CreditComparator creditComparator = new CreditComparator();
                String[] labelCredit1 = view.getConnected().getPathViewer().getLabels()[1].getText().split(": ");
                String[] labelCredit2 = view.getConnected().getPathViewer().getLabels()[2].getText().split(": ");
                
                if(!creditComparator.creditAbove58(labelCredit1, labelCredit2))
                {
                    view.getConnected().getPathViewer().setMessage("Vous devez avoir au moins 58 crédits !", Color.RED);
                }
                else if(!creditComparator.creditQ1CloseToQ2(labelCredit1, labelCredit2))
                {
                    view.getConnected().getPathViewer().setMessage("Vous devez avoir un nombre équivalent entre les 2 quadrimestres", Color.red);
                }
                else
                {
                    action = master.validateYearPlanning();
                    master.setPathViewer(view.getConnected().getPathViewer()); 
                }
                break;
            case "PathViewer":
                master.setPathViewer(view.getConnected().getPathViewer());
                break;
            case "Supprimer":
                master.deleteCurrentYearPlanning();
                master.assignStudents(view.getConnected().getMyStudents().getObserver());
                action = "MyStudents";
                break;
            case "Modifier":
                view.getConnected().getCreationPath().setAvailableCourses(master.checkAvailableCourse(false));
                view.getConnected().getCreationPath().setCourseToSemester(master.checkAvailableCourse(true));
                action = "CreationPath";
                break;
            default:

        }
        changerCurrentConnectedWindow(action);
    }
    
    /**
     * This method allows to login
     *
     * @param pseudo the name of the user
     * @param password the password of the user
     */
    public void login(String pseudo, String password) {
        LevelAccess access = master.login(pseudo, password);
        if (access != null) {
            if (access.getLevelname().equals("student")) {
                changeConnectedWindow("PathViewer");
                master.setPathViewer(view.getConnected().getPathViewer());
            } else {
                changeConnectedWindow("MyStudents");
                master.assignStudents(view.getConnected().getMyStudents().getObserver());
            }
            view.changeWindow("Connected");

            login.replyConnectionRequest(true);
        }
        login.replyConnectionRequest(false);
        view.getConnected().setUserName(this.master.getFullName());
    }

    /**
     * Disconnect and goes back to login windows
     */
    public void deconnection() {
        view.getConnected().getCreationPath().clear();
        view.getConnected().getPathViewer().clear();
        master.deconnection();
        view.changeWindow("Login");
    }

    /**
     * This allows a teacher to see the path of a student
     *
     * @param studentSearch the fullname of the student
     */
    public void showStudentPathForTeacher(String studentSearch) {
        master.setCurrentStudent(studentSearch);
        master.fillStudentPath(view.getConnected().getPathViewer().getPanels(), view.getConnected().getPathViewer().getLabels());
        view.getConnected().getPathViewer().panelProf(true);
        changerCurrentConnectedWindow("PathViewer");

    }

    public PathViewer getConnectedPathViewer(){
        return view.getConnected().getPathViewer();
    }
    

    private void changerCurrentConnectedWindow(String window) {
        view.getConnected().changeWindow(window);
    }

    private void changeConnectedWindow(String window) {
        view.getConnected().changeAccueil(window);
        changerCurrentConnectedWindow(window);
    }

}
