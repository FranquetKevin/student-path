/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import datasources.CoursesPackageManager;
import datasources.SectionManager;
import datasources.StudentManager;
import datasources.hibernate.HibernateConnectionManager;
import datasources.hibernate.HibernateCousesPackageManager;
import datasources.hibernate.HibernateSectionManager;
import datasources.hibernate.HibernateStudentManager;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JLabel;
import javax.swing.JPanel;
import models.YearPlanning;
import models.TeachingUnit;
import models.LevelAccess;
import models.Section;
import models.Student;
import models.StudentPath;
import models.StudentsList;
import models.TeachingUnitState;
import models.TypeYearPlanning;
import models.YearSuccessChangedEvent;
import models.remplissage.FillBlock;
import views.connecte.Course;
import views.connecte.PathViewer;

/**
 *
 * @author David
 */
public class MasterController implements Observer{

    private final HibernateConnectionManager connection;
    private final StudentManager students;
    private final SectionManager sections;
    private final CoursesPackageManager courses;
    
    private final ViewsController view;
    
    private LevelAccess access;
    private final StudentsList studentsList;
    private YearPlanning currentBlock;
    private Student currentStudent;
    
    private StudentPath studentPath;
    
    

    /**
     *  Default builder, get everything ready
     */
    public MasterController() {
        connection = new HibernateConnectionManager();
        students = new HibernateStudentManager(connection);
        sections = new HibernateSectionManager(connection);
        courses = new HibernateCousesPackageManager(connection);
        LevelAccess.setSectionManager(sections);
        studentsList = new StudentsList();
        view = new ViewsController(this);
    }

    /**
     * this method gives the levelAccess of a user connecting
     * @param pseudo the name of the person trying to connect
     * @param motDePasse the password of the person
     * @return the LevelAccess of the user
     */
    public LevelAccess login(String pseudo, String motDePasse) {
        try {
            access = new LevelAccess(pseudo);
            if(!access.connect(motDePasse)) {
                access = null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            access = null;
        }
        return access;
    }

    /**
     *Get all available Courses
     * @return all the Available courses in a list
     */
    public List<TeachingUnit> availableCourse() {
        Section s = currentStudent.getSection();
        return courses.getCoursesFrom(s);
    }

    /**
     *Add a course to the path of the student
     * @param cours the course to add
     * @return true if ok, false if nok
     */
    public boolean addCoursePath(TeachingUnit cours) {
       studentPath = this.currentStudent.getPath();
        return studentPath.addCoursesPackage(cours);
    }

    /**
     *Remove a course from the path of the student
     * @param cours the course to remove from the path
     * @return true if ok, false if nok
     */
    public boolean removeCoursePath(TeachingUnit cours) {
        studentPath = this.currentStudent.getPath();
        return studentPath.removeCoursesPackage(cours);
    }

    /**
     *Save the CurrentPath
     */
    public void validatePath() {
        saveStudent();
        if(access.getLevelname().equals("student")){
            setPathViewer(view.getConnectedPathViewer());
            view.action("PathViewer");
        }
        else{
            view.showStudentPathForTeacher(currentStudent.getFullName());
        }
    }

    /**
     *Set an observer to the list
     * @param observer the observer of the list
     */
    public void assignStudents(Observer observer) {
        studentsList.addObserver(observer);
        List<Student> stds = students.getAllToValidate();
        studentsList.setStudents(stds);
    }

    /**
     *Get the Student List
     * @return an instance of StudentsList
     */
    public StudentsList getStudentsList() {
        return studentsList;
    }

    /**
     *Set the viwer of the path of a student
     * @param panels the panels to show the information
     * @param labels the labels who will be shown
     */
    public void setPathViewer(PathViewer pathViewer) {

        currentStudent = students.get(access.getSerial());
        
        
        if (currentStudent == null) {
            currentStudent = new Student(access.getSerial(), 
                    access.getSection(),access.getFirstname(), 
                    access.getSurname(), new StudentPath()
            );
        }
        else if(access.getLevelname().equals("student")){
            currentBlock = currentStudent.getPath().getCurrentYearPlanning();
            if(currentBlock.getStatus() == TypeYearPlanning.VALIDATE_STUDENT){
                pathViewer.hideModification();
                pathViewer.setMessage("Le parcours doit encore être validé par le professeur référent.", Color.red);
            }
            else if(currentBlock.getStatus() == TypeYearPlanning.VALIDATE){
                pathViewer.hideModification();
                pathViewer.setMessage("Le parcours a été validé et sera celui de votre année.", Color.green);
            }
        }
        setCurrentBlock();
        fillStudentPath(pathViewer.getPanels(), pathViewer.getLabels());
    }

    /**
     *Show the studentPath's Datas
     * @param panels the panels to show the information
     * @param labels the labels who will be shown
     */
    public void fillStudentPath(JPanel[] panels, JLabel[] labels) {
            FillBlock filler = new FillBlock(this.access.getLevelname().equalsIgnoreCase("Student"));
            filler.fillCourses(this,currentBlock, panels, labels);
            
    }
    /**
     *Set the currentBlock
     */
    public void setCurrentBlock(){
        studentPath = this.currentStudent.getPath();
        currentBlock = currentStudent.getPath().getCurrentYearPlanning();
    }
    
    /**
     *Save the student
     */
    public void saveStudent() {
        students.save(currentStudent);
    }

    /**
     *Set the current student
     * @param studentSearch the id of the current student
     */
    public void setCurrentStudent(String studentSearch) {
        
        for(Student aStudent:studentsList.getStudents()){
            if(aStudent.getFullName().equalsIgnoreCase(studentSearch)){
                currentStudent = aStudent;
            }
        }
        setCurrentBlock();
    }
    
    public List<TeachingUnit> checkAvailableCourse(boolean state){
        
        setCurrentBlock();
        List<TeachingUnit> coursTemp = new ArrayList<>();
        
        if(!currentBlock.getPackages().isEmpty()){
            for(TeachingUnit teachingUnit : availableCourse()){
                if(isTheTeachingUnitInCurrentBlock(teachingUnit) == state){
                    coursTemp.add(teachingUnit);
                }
            }
            return coursTemp;
        }
        else{
            if(state){
                return coursTemp;
            }
            else{
                return availableCourse();
            }
        }
    }
    
    private boolean isTheTeachingUnitInCurrentBlock(TeachingUnit teachingUnit){
        boolean state = false;
        for(TeachingUnitState teachingUnitState : currentBlock.getPackages()){
            if(teachingUnitState.getUnit().equals(teachingUnit)){
                state = true;
            }
        }
        
        return state;
    }
    
    /**
     * Get the full name of the user
     * @return the name and the surname, séparated by a space
     */
    public String getFullName() {
        String result;
        try{
            result = this.access.getFirstname()+" "+this.access.getSurname();
        }catch(Exception ex){
            result = "Erreur lors de la récupération du nom";
        }
        return result;
    }
    
    /**
     * Get the levelname of the user
     * @return the levelname
     */
    public String getLevelname(){
        return this.access.getLevelname();
    }
    
    /**
     * Validate the year planning of the student
     * The year planning is set at VALIDATE_STUDENT if it's the student who validates
     * and at VALIDATE if it's the professor who validates
     * @return the next view name
     */    
    public String validateYearPlanning(){
        currentBlock = this.studentPath.getCurrentYearPlanning();
        String nextView;
        if(access.getLevelname().equals("student")){
            currentBlock.setStatus(TypeYearPlanning.VALIDATE_STUDENT);
            nextView = "PathViewer";
        }
        else{
            currentBlock.setStatus(TypeYearPlanning.VALIDATE);
            nextView = "MyStudents";
        }
        students.save(currentStudent);
        return nextView;
    }
    
    public void deconnection(){
        access = null;
        currentBlock = null;
        currentStudent = null;
        connection.close();
    }
    
    public void deleteCurrentYearPlanning(){
        students.deleteCurrentYearPlanning(currentStudent);
    }


    @Override
    public void update(Observable o, Object arg) {
        YearSuccessChangedEvent event = (YearSuccessChangedEvent) o;
        event.getUnit().isSuccessful(event.isYearSuccess());
        this.students.save(currentStudent);
    }
}
