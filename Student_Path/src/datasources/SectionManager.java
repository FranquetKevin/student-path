/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datasources;

import models.Section;

/**
 *
 * @author Thomas Menini
 */
public interface SectionManager extends DataManager<Section> {
    
}
