/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datasources.hibernate;

import datasources.SectionManager;
import java.io.Serializable;
import java.util.logging.Logger;
import models.Section;

/**
 *
 * @author Thomas Menini
 */
public class HibernateSectionManager implements SectionManager {
    
    private static final Logger logger = Logger.getLogger(HibernateSectionManager.class.getName());
    private final HibernateConnectionManager manager;

    /**
     * Default builder
     * @param manager the hibernateManager
     */
    public HibernateSectionManager(HibernateConnectionManager manager) {
        this.manager = manager;
    }

    @Override
    public void save(Section data) {
        
    }

    @Override
    public Section get(Serializable id) {
        return (Section) manager.get(Section.class, id);
    }
    
}
