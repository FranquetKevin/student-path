/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datasources.hibernate;

import java.io.Serializable;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 *
 * @author Menini Thomas (d120041) <t.menini@student.helmo.be>
 */
public class HibernateConnectionManager {

    private Configuration configuration;
    private SessionFactory factory;
    private Session session;

    /**
     * This method configure the connection with hibernate, using default parameters
     * @return the parametred session factory
     */
    public SessionFactory setupConnection() {
        configuration = new Configuration().configure("datasources/hibernate/hibernate.cfg.xml");
        configuration.setProperty("hibernate.temp.use_jdbc_metadata_defaults","false");
        ServiceRegistry service = new ServiceRegistryBuilder()
                .applySettings(
                        configuration.getProperties()
                ).buildServiceRegistry();
        factory = configuration.buildSessionFactory(service);
        return factory;
    }

    /**
     * return the session
     * @return a session
     */
    public Session getSession() {
        if (factory == null) {
            setupConnection();
        }
        if (session == null) {
            session = factory.openSession();
        }
        return session;
    }

    /**
     * check if the session is valid and usable
     * @return a boolean describing the validity of the connection
     */
    public boolean isConnectionValid() {
        if (session == null) {
            return false;
        }
        return session.isConnected();
    }
    
    /**
     * Allows to get a serializable object from the database
     * @param type the type of the object
     * @param id the id
     * @return the object asked frmo the database
     */
    public Object get(Class type, Serializable id) {
        return getSession().get(type, id);
    }

    /**
     * Save an object into the database
     * @param objet the object to save
     */
    public void save(Object objet) {
        if (session != null) {
            session.saveOrUpdate(objet);
        }
    }

    /**
     * Delete an object from the database
     * @param object the object to delete
     */
    public void delete(Object object) {
        if (session != null) {
            session.delete(object);
        }
    }

    /**
     * close the session
     */
    public void close() {
        if (session != null) {
            session.close();
            session = null;
        }
    }

    /**
     * get the current transaction
     * @return the transaction
     */
    public Transaction getTransaction() {
        return getSession().getTransaction();
    }

    /**
     * Begin a transaction
     * @return the transaction
     */
    public Transaction begin() {
        getTransaction().begin();
        return getTransaction();
    }

    /**
     * Allows to commit the transaction
     * @return the transaction
     * @throws Exception if there is a problem with the commit
     */
    public Transaction commit() throws Exception {
        if (!getTransaction().wasCommitted()) {
            getTransaction().commit();
        }
        return getTransaction();
    }

    /**
     * Do a rollback of the transaction
     * @return the transaction
     * @throws Exception if there is a problem with the rollback
     */
    public Transaction rollback() throws Exception {
        if (!getTransaction().wasRolledBack()) {
            getTransaction().rollback();
        }
        return getTransaction();
    }

    /**
     * close the factory
     */
    public void shut() {
        factory.close();
    }

    private String from(Class objet) {
        return "from " + objet.getSimpleName();
    }

    /**
     * Return a list of all the object with the same class from the database
     * @param objet the class to get
     * @return a list
     */
    public List list(Class objet) {
        return getSession().createQuery(from(objet)).list();
    }
}
