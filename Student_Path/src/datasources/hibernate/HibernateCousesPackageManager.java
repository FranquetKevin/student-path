/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datasources.hibernate;

import datasources.CoursesPackageManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.TeachingUnit;
import models.Dependencie;
import models.Section;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Thomas Menini
 */
public class HibernateCousesPackageManager extends DataManager<TeachingUnit> implements CoursesPackageManager {
    
    private static final Logger logger = Logger.getLogger(HibernateCousesPackageManager.class.getName());

    /**
     * Default builder
     * @param manager
     */
    public HibernateCousesPackageManager(HibernateConnectionManager manager) {
        super(manager);
    }
    
    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void save(TeachingUnit data) {
        try {
            manager.begin();
            for (Dependencie dependencie : data.getDependencies()) {
                manager.save(dependencie);
            }
            manager.save(data);
            manager.commit();
        } catch (Exception ex) {
            logger.log(Level.FINE, data.getName()+" n'a pas pu être sauvegardé", ex);
            try {
                manager.rollback();
            } catch (Exception ex1) {
                logger.log(Level.SEVERE, "la transaction n'a pu être annulée : la base de données est actuellement instable", ex1);
            }
        } finally{
            manager.close();
        }
    }

    @Override
    public TeachingUnit get(Serializable id) {
        return (TeachingUnit) manager.get(TeachingUnit.class, id);
    }

    @Override
    public List<TeachingUnit> getCoursesFrom(Section s) {
        if (s == null) return new ArrayList<>();
        Session session = manager.getSession();
        Query q = session.createQuery("from models.TeachingUnit where fk_id_section = :id_section");
        q.setParameter("id_section", s.getId());
        return q.list();
    }
    
}
