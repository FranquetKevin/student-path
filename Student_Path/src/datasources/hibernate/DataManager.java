/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datasources.hibernate;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * class using the interface DataManager
 * @author Thomas Menini
 * @param <T>
 */
public abstract class DataManager<T> implements datasources.DataManager<T> {
    
    /**
     * the manager of the connection to the database
     */
    protected final HibernateConnectionManager manager;

    /**
     * default constructor
     * @param manager The manager of Hibernate connection
     */
    public DataManager(HibernateConnectionManager manager) {
        this.manager = manager;
    }

    /**
     * perform a RollBack on the database
     */
    protected void rollback() {
        try {
            manager.rollback();
        } catch (Exception ex1) {
            getLogger().log(Level.SEVERE, "la transaction n'a pu être annulée : la base de données est actuellement instable", ex1);
        }
    }
    
    /**
     * return the logger
     * @return the logger
     */
    protected abstract Logger getLogger();
}
