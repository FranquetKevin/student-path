/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datasources.hibernate;

import datasources.StudentManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.YearPlanning;
import models.TeachingUnit;
import models.Student;
import models.TeachingUnitState;
import models.TypeYearPlanning;
import org.hibernate.Query;

/**
 *
 * @author Thomas Menini
 */
public class HibernateStudentManager extends DataManager<Student> implements StudentManager {

    private static final Logger logger = Logger.getLogger(HibernateStudentManager.class.getName());

    /**
     * The default builder
     *
     * @param manager The hibernate manager
     */
    public HibernateStudentManager(HibernateConnectionManager manager) {
        super(manager);
    }

    @Override
    public List<Student> getAllToValidate() {
        List<Student> result = new ArrayList<>();
        List<Student> liste = (List<Student>) manager.list(Student.class);
        for (Student aStudent : liste) {
            if (aStudent.getPath().getCurrentYearPlanning().getStatus() == TypeYearPlanning.VALIDATE_STUDENT) {
                result.add(aStudent);
            }
        }
        return result;
    }

    @Override
    protected Logger getLogger() {
        return logger;
    }

    @Override
    public void save(Student student) {
        if (student != null) {
            try {
                manager.begin();
                for (YearPlanning year : student.getPath().getPath()) {
                    manager.save(year);
                    for (TeachingUnitState unit : year.getPackages()) {
                        manager.save(unit);
                    }
                }
                manager.save(student);
                manager.commit();
            } catch (Exception ex) {
                getLogger().log(Level.FINE, student.getFullName() + " n'a pas pu être sauvegardé", ex);
                rollback();
            } finally {
                manager.close();
            }
        }
    }

    @Override
    public Student get(Serializable matricule) {
        return (Student) manager.get(Student.class, matricule);
    }
    
    @Override
    public void deleteCurrentYearPlanning(Student currentStudent){
        currentStudent.getPath().getCurrentYearPlanning().removeAllPackage();
        currentStudent.getPath().getCurrentYearPlanning().setStatus(TypeYearPlanning.IN_PROGRESS);
        this.save(currentStudent);
    }
}
