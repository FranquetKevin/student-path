/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datasources;

import java.util.List;
import models.Student;

/**
 *
 * @author Thomas Menini
 */
public interface StudentManager extends DataManager<Student> {


    public List<Student> getAllToValidate();
    
    public void deleteCurrentYearPlanning(Student currentStudent);
}
