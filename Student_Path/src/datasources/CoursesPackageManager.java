/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datasources;

import java.util.List;
import models.TeachingUnit;
import models.Section;

/**
 *
 * @author Thomas Menini
 */
public interface CoursesPackageManager extends DataManager<TeachingUnit> {

    /**
     * This method collect all the courses of a section
     * @param s the section where to find the courses
     * @return a list of the CoursesPackage
     */
    List<TeachingUnit> getCoursesFrom(Section s);
}
