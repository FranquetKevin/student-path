/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datasources;

import java.io.Serializable;

/**
 * Interface to manage all the datas, providing save and get
 * @author Thomas Menini
 * @param <T> the type of the data
 */
public interface DataManager<T> {

    /**
     * Save a data
     * @param data the data to save
     */
    public void save(T data);

    /**
     * Get a data
     * @param primaryKey the key of the data
     * @return the data
     */
    public T get(Serializable primaryKey);
}
